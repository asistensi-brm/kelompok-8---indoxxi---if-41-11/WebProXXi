<h1>The Digital Millennium Copyright Act (DMCA)</h1>

<div class="badge" >
	<div>
		<img src="<?php echo base_url() ?>assets/img/icon/indo.png" alt="bendera" style="border : 0; vertical-align:middle;width:auto; height:28px;"/>

		Bahasa Indonesia
	</div>
</div>
	<p>
		IndoXXI atau XX1 tidak memiliki atau menyimpan konten di server sendiri, hanya mengambil tautan atau embed konten dari luar yang telah di upload ke situs-situs website populer seperti Youtube, Google Drive, Dailymotion, ZShare dan sejenisnya.
	</p>
	<p>
		<li>Spesifikasikan konten hak cipta yang menurut anda telah dilanggar,</li>
    	<li>Jika anda mengklaim pelanggaran dari beberapa karya cipta dalam satu email maka silahkan menuliskan daftar tersebut secara detail beserta alamat website yang berisikan konten yang melanggar tersebut,</li>
    	<li>Informasikan nama anda, telepon, alamat kantor dan alamat email untuk memungkinkan kami menghubungi anda,</li>
    	<li>Jika anda memiliki itikad yang baik maka kirimkan email tersebut sendiri, tidak diizinkan apabila email tersebut dari agen atau pihak ketiga,</li>
    	<li>Informasi yang dituliskan harus akurat dan berada dibawah hukuman pemalsuan.</li>

	</p>


<div class="badge">
	<div>
		<img src="<?php echo base_url() ?>assets/img/icon/english.png" alt="bendera" style="border : 0; vertical-align:middle;width:auto; height:28px;"/>

		English
	</div>
</div>
	<p>
		IndoXXI or XX1 is an online website service provider like defined in Digital Millenium Copyright Act. We respect the copyright laws and will protect the right of every copyright owner seriously. If you are the owner of any content showed on IndoXXI or XX1 and you dont want to allow us to use the content, then you are very able to tell us by email to xxidmca@gmail.com so that we can identify and take necessary action. We cannot take any action if you dont give us any information about it, so please send us an email with the details like:
	</p>
	<p>
		<li>Specification about copyright of content which claimed to be infringed, </li>
    	<li>If you claimed about infringement from some copyright works in one email, please write the list about it in detail including website urls contain exact content that claimed to be infringing,</li>
    	<li>Give us information about your name, phone, office address and email address also to allow us contact you if neccessary, </li>
    	<li>We really expect that the sender would be real copyright owner and not the third party or agents, </li>
    	<li>Information written must be accurate and under the law of counterfeiting. </li>
	</p>
