<html>

<head>
    <?php
      if ($this->session->flashdata('judulFilm')) {
        ?>
        <title><?php echo $this->session->flashdata('judulFilm')?> | INDOXXI</title>
        <?php
      }
     ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css' ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/movie_focus.css' ?>" />
