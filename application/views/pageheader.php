    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/headerfooter.css' ?>" />
  	<script src="<?php echo base_url().'assets/js/jquery.min.js' ?>"></script>
    <script src="<?php echo base_url().'assets/js/header.js' ?>"></script>
    <script>
    window.onload = function() {
      console.log(window.localStorage.getItem("token"));

      isLoggedin();
      setbaseurl(document.getElementById("base").value);
      // buat function onLoad di js untuk dijalankan pertamakali
      onLoad();
    }
    function isLoggedin(){
      console.log("loading");
      let login = document.getElementById('loginprofile');
      let sw = document.getElementById('whistlistbutton');

      if(window.localStorage.getItem("token") != null){
        console.log("on working");
        $.post( "http://localhost:3000/isLogin", { token: window.localStorage.getItem("token")}).done(function( data ) {
          console.log(data);
          window.localStorage.setItem("uid" , data.id);
          if(data.isLoggedin){
            login.href = "<?php echo base_url() ?>profile";
            login.children[0].innerHTML = "Profile";

          }else{

            login.href = "<?php echo base_url() ?>login";
            login.children[0].innerHTML = "Login";
            sw.remove();
          }

        });
      }else{
        console.log("kosong");
        window.localStorage.setItem("uid" , -1);
        login.href = "<?php echo base_url() ?>login";
        login.children[0].innerHTML = "Login";
        sw.remove();
      }
    }
    </script>
</head>
<body>

<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<div id="header" >
  <div>

    <a href="<?php echo base_url()."home" ?>" ><img src="https://img.akubebas.com/images/indoxxi-logo-satu.gif" style="float:left;margin-right:3%;"/></a>



    <!--<button id="login" class="">Login</button>
    <button id="profil" class="drpdown">Profil</button>
    <button class="active">Home</button>-->
    <div class="drpdown" style="float:left;">
      <a href="#"><button class="dropbtn" href="#">Bioskop 21</button></a>
      <div class="drpdown-content" style="background-color: #fcfcfc;" >
        <a href="#" onclick="toPageSearchbyOrder('terbaru')">Featured</a>
        <a href="#" onclick="toPageSearchbyOrder('rating')">High Rating</a>
        <a href="#" onclick="toPageSearchbyOrder('trending')">Trending</a>

        <a href="#" id="whistlistbutton" onclick="toPageSearchbyOrder('whistlist')">whistlist</a>
      </div>
    </div>
    <div class="drpdown" style="float:left;">
      <a href="#"><button class="dropbtn" href="#">Serial TV</button></a>
      <div class="drpdown-content" style="background-color: #fcfcfc;" >
        <a href="#" onclick="toPageSearch({name : '',  nation :'china,hongkong,india,japan,korea,taiwan,thailand,vietnam,australia,malaysia,iran',genre : 'Drama',orderby : 'terbaru',page : 1})">Drama Asia</a>
        <a href="#" onclick="toPageSearch({name : '',  nation :'korea',genre : 'Drama',orderby : 'terbaru',page : 1})">Drama Korea</a>
        <a href="#" onclick="toPageSearch({name : '',  nation :'',genre : 'Animation',orderby : 'terbaru',page : 1})">Anime & Kartun</a>
      </div>
    </div>
    <div class="drpdown" style="float:left;">
      <a href="#"><button class="dropbtn" href="#">Layar Kaca</button></a>
      <div class="drpdown-content" style="background-color: #fcfcfc;" >
        <a href="#" onclick="toPageSearchbyNation('china,hongkong,india,japan,korea,taiwan,thailand,vietnam,australia,malaysia,iran')">Film Asia</a>
        <a href="#" onclick="toPageSearchbyNation('france,uk,usa,germany')">Film Barat</a>
        <a href="#" onclick="toPageSearchbyNation('india')">Film India</a>
      </div>
    </div>
    <div class="drpdown" style="float:left;">
      <a href="#"><button class="dropbtn">Genre</button></a>
      <div class="drpdown-content" style="background-color: #fcfcfc;" >
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Action</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Adventure</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Animation</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Comedy</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Crime</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Drama</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Fantasy</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Horor</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Mystery</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Romance</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Sci-fi</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Thriller</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Familly</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">Sport</a>
        <a href="#" onclick="toPageSearchbyGenre(this.innerHTML)">War</a>
      </div>
    </div>
    <div class="drpdown" style="float:left;">
      <a href="#"><button class="dropbtn">Negara</button></a>
      <div class="drpdown-content" style="background-color: #fcfcfc;" >
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">China</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">France</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">Hongkong</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">India</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">Japan</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">Korea</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">Taiwan</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">Thailand</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">UK</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">USA</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">Vietnam</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">Germany</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">Australia</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">Iran</a>
        <a href="#" onclick="toPageSearchbyNation(this.innerHTML)">Malaysia</a>
      </div>
    </div>

    <div class="drpdown" style="float:left;">
      <a href="#"><button class="dropbtn highlight" onclick="toPageSearch({name : '',  nation :'',genre : 'Animation',orderby : 'terbaru',page : 1})">Anime</button></a>
    </div>

    <div class="drpdown" style="float:left;">
      <a href="<?php echo base_url().'privasi/dmca'?>"><button class="dropbtn" style="width:70%;"><img src="https://img.akubebas.com/images/dmca.png" style="object-fit: fill;width:90%;height:60%" ></button></a>
    </div>

    <div style="height:100%;width: 28%;position:relative;float:right;">

        <div style="position:relative; left:0;height:50%;width:65%;margin-top:4.5%;">
          <input type="search" value="" id="search" placeholder="Search" class="search-input" >

          <button type="submit" onclick="toPageSearchbyName(this.parentElement.children[0].value)">
            <img src="https://static.thenounproject.com/png/101791-200.png" style="object-fit: fill;width:100%;height:100%"/>
          </button>

        </div>
        <div class="drpdown" style="float:right;position:absolute;right:0;top:0;height: 50%;margin-top:4.5%;margin-right:20px;">
          <a href="#" id="loginprofile"><button class="dropbtn" style="margin-top:0;height:100%;" ></button></a>
        </div>


    </div>




    </div>


  </div>
</div>
