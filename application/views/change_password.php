<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Profile</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css' ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/login-register.css' ?>" />
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php echo base_url().'assets/js/login-register.js' ?>"></script>

</head>
<body style="background:#00c6ff">
	<div class="register" style="width:100%; height:100%; margin:0; padding-right:0;">
        <div class="row" >
            <div class="col-md-3 register-left">
                <img src="<?php echo base_url().'assets/img/login/movie_creation_white_192x192.png' ?>" alt=""/>
                <h4>Welcome to Indoxii</h4>
                <p>Get benefits with become our member!</p>
                <a href="<?php echo base_url('home') ?>"><input type="button" value="Home" style="color: #0062cc;"/><br/></a>
            </div>
            <div class="col-md-9 register-right" style="padding-right: 2%;">
				<div class="tab-content" id="myTabContent" name="Login" >
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab" style="height:470px;">
                        <h3 class="register-heading">Profile</h3>
                        <div class="row register-form"">
                        	<form action="<?php echo base_url().'login/login'; ?>" method="post">
			                                <div class="row register-form" style="text-align:center;">

																				<div class="form-group" style="width:200%;">
																						<input type="text" class="form-control" placeholder="Password Lama *" name="passlama" value="" />
																				</div>
																				<div class="form-group" style="width:200%;">
																						<input type="password" class="form-control" placeholder="Password Baru *" name="passbaru" value="" />
																				</div>
																				<div class="form-group" style="width:200%;">
																						<input type="password" class="form-control" placeholder="Konfirmasi Password Baru *" name="konfirmasipassbaru" value="" />
																				</div>

																				<input type="submit" class="btnRegister" value="Login" style="margin: auto;width:200px;height:50px;padding:0;"/>
			                                </div>
																		</form>>
                        </div>
                    </div>
				</div>
            </div>
        </div>
	 </div>
</body>
</html>
