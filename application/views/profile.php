<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Profile</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/home.css' ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css' ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/profile.css' ?>" />
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php echo base_url().'assets/js/profile.js' ?>"></script>
	<script>
		window.onload = function(){
			if(window.localStorage.getItem("token") != null){
        console.log("on working");
        $.post( "http://localhost:3000/isLogin", { token: window.localStorage.getItem("token")}).done(function( data ) {

          window.localStorage.setItem("uid" , data.id);
					console.log(data.id);
          if(data.isLoggedin){
						loadData(data.data);
          }else{
            window.location.href = "<?php echo base_url() ?>";
          }

        });
      }else{
				window.location.href = "<?php echo base_url() ?>";
      }
		}
	</script>

</head>
<body style="background:linear-gradient(to bottom, #845422, #6a3b24, #4b2621, #291718, #000000);position:relative;">
	<div class="card" style="position:absolute;background:#fff;  width: 20%;left:0; padding:3%;padding-top:18%;">

			<button type="button" name="button" class="cardBtn" style="border-top:none" onclick="changeView(0)">Account data</button>
			<button type="button" name="button" class="cardBtn" onclick="changeView(1)">Edit data</button>
			<button type="button" name="button" class="cardBtn" onclick="changeView(2)">Whistlist</button>
			<button type="button" name="button" class="cardBtn" onclick="window.location.href='<?php echo base_url().'home'; ?>'">Back to Home</button>

	</div>

	<div class="card" id="card" style="position:absolute;background:#fff;width: 70%;right:0; padding:5%;">
		<div class="tab-pane fade show " id="showData" role="tabpanel" aria-labelledby="home-tab" style="height:470px;display:block;">
			 <h3 class="register-heading" style="margin-bottom: 5%;">Your Data</h3>

				 <div class="row register-form">
						 <div class="col-md-6" >
								 <div class="form-group">
									 	<label><strong>Nama</strong></label><br>
										<label id="dnama">Nama user</label>
								 </div>
								 <div class="form-group">
									 <label><strong>Username</strong></label><br>
									 <label id="duname">Username User</label>
								 </div>
								 <div class="form-group">
									 	<label><strong>Password</strong></label><br>
										<label id="dpass" >******</label>
								 </div>

						 </div>
						 <div class="col-md-6">
								 <div class="form-group">
									 	<label><strong>Email</strong></label><br>
										<label id="demail">testEmail@gmail.com</label>
								 </div>
								 <div class="form-group">
										 <label><strong>Phone</strong></label><br>
										 <label id="dnomor">08123</label>
								 </div>
								 <div class="form-group">
									 	<label><strong>Gender</strong></label><br>
										<label id="dgender">Male</label>
								 </div>

						 </div>
						 <input type="button" class="btnRegister"  onclick="Logout()" value="Logout" style="background: #E14242;margin: auto;margin-top: 10%;width:200px;height:50px;padding:0;"/>
						 <input type="button" class="btnRegister"  onclick="changeView(1)" value="Edit Data" style="margin: auto;margin-top: 10%;width:200px;height:50px;padding:0;"/>
				 </div>

	 </div>
		<div class="tab-pane fade show " id="editData" role="tabpanel" aria-labelledby="home-tab" style="height:470px;display:none;">
			 <h3 class="register-heading"  style="margin-bottom: 5%;">Edit Your Data</h3>
			 <form action="<?php echo base_url().'profile/updateData'; ?>" method="post">
				 <div class="row register-form">
						 <div class="col-md-6" >
								 <div class="form-group">
									 		<input type="hidden" class="form-control" id="iid" placeholder="Your Name *" name="id" value="" />
										 <input type="text" class="form-control" id="inama" placeholder="Your Name *" name="nama" value="" />
								 </div>
								 <div class="form-group">
										 <input type="text" class="form-control" id="iuname" placeholder="Username *" name="username" value="" />
								 </div>
								 <div class="form-group">
										 <input type="password" class="form-control" id="iopass" placeholder="Insert Your Password to Confirm *" name="opassword" value="" />
								 </div>

								 <label>Ganti Password?</label>
								 <div class="form-group">
										 <div class="maxl" id="ipassChange" >
												 <label class="radio inline">
														 <input type="radio"  name="passChange" value="No" checked>
														 <span> No </span>
												 </label>
												 <label class="radio inline">
														 <input type="radio" name="passChange" value="Yes">
														 <span> Yes </span>
												 </label>
										 </div>
								 </div>
								 <label style="font-size:10;">*Tidak perlu mengisi ini jika tidak ingin mengganti password anda </label>
								 <div class="form-group">
										 <input type="password" class="form-control" id="ipass" placeholder="New Password *" name="password" value="" />
								 </div>
								 <div class="form-group">
										 <input type="password" class="form-control" id="icpass" placeholder="Confirm Password *" name="cpassword" value="" />
								 </div>

						 </div>
						 <div class="col-md-6">
								 <div class="form-group">
										 <input type="email" class="form-control"  id="iemail" placeholder="Your Email *" name="email" value="" />
								 </div>
								 <div class="form-group">
										 <input type="text" minlength="10" maxlength="13"  id="inomor"  class="form-control" placeholder="Your Phone *" name="hp" value="" />
								 </div>
								 <div class="form-group">
										 <div class="maxl" id="igender" >
												 <label class="radio inline">
														 <input type="radio"  name="gender" value="male">
														 <span> Male </span>
												 </label>
												 <label class="radio inline">
														 <input type="radio" name="gender" value="female">
														 <span>Female </span>
												 </label>
										 </div>
								 </div>

						 </div>
						 <input type="submit" class="btnRegister"  value="Update" style="margin: auto;width:200px;height:50px;padding:0;"/>
				 </div>
			 </form>

	 </div>
	 <div style="display:none;height:100%;position:relative;">
		 <label id="nextlab" ><strong>Your Whistlist</strong></label>
		 <h6 id="nextlab" class="udr" style="float:right;" onclick="toPageSearchbyOrder('whistlist','<?php echo base_url() ?>')">Filter Whistlist >></h6>
		 <div id="whistlist" style="padding-bottom: 30px;overflow-y: scroll;width:100%;height:90%;" >

		 </div>
		 <div id="nextbtn" style="position:relative;" >
			 <div style="position:absolute;left:50%; transform: translate(-50%, 0);height:40px;">
				 <button onclick="nextPage(-1)" class="btnNext"><</button>
				 <button class="btnNext" id="current" >5</button>
				 <button onclick="nextPage(1)" class="btnNext">></button>
			 </div>
		 </div>
 		</div>

	</div>
</body>
</html>
