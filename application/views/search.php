<div style="width:92%; padding-left:30px;padding-right:30px;margin:2%;border-radius:1em;padding-top:20px;">
  <div id="searchFilterBy">
    <div class="tabHome2" style="float:left;background:#F4B642;" id="nam">
      <button disabled style="height:inherit; background: #686868;color: #fff;border:none;" >Hasil Pencarian "..." </button>
    </div>
    <div class="tabHome2" style="float:left;background:#F4B642;" id="ord">
      <button disabled style="height:inherit; background: #686868;color: #fff;border:none;" >Terbaru </button>
    </div>
    <div class="tabHome2" style="float:left;background:#F4B642;" id="genr">
      <button disabled style="height:inherit; background: #686868;color: #fff;border:none;" >Genre </button>
    </div>
    <div class="tabHome2" style="float:left;background:#F4B642;" id="nat">
      <button disabled style="height:inherit;background: #686868;color: #fff;border:none;" >Nation </button>
    </div>
    <div class="tabHome" style="float:right;background:rbga(0,0,0,0);">
      <button style="height:inherit;width:110px; margin-left:8%;background: #686868;color: #fff;border:none;" onclick="opencloseFilter()">Filter </button>
    </div>
  </div>
  <br style="clear:both;">
  <div id="filter" style="background:rgba(0,0,0,0.2);padding:10px;display:none;">

    <div style="width:25%; float:left;">
      <h4>Urut Berdasarkan</h4>
      <div style="background:rgba(255,255,255,0.5);padding:10px;" id="orderby">
        <input type="radio" name="gender" value="terbaru" checked> Terbaru<br>
        <input type="radio" name="gender" value="populer"> Populer<br>
        <input type="radio" name="gender" value="rating"> Rating<br>
        <input type="radio" name="gender" value="whistlist"> whistlist
      </div>
    </div>
    <div style="width:70%;float:right;">
      <h4>Negara</h4>
      <div style="background:rgba(255,255,255,0.5);padding:10px;padding-bottom:5px;" id="nations">
        <div style="float:left;"><input type="checkbox" name="nation" value="USA" style="margin-bottom:10px;" > USA</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="UK" style="margin-bottom:10px;" > UK</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Australia" style="margin-bottom:10px;"> Australia</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="China" style="margin-bottom:10px;"> China</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Philippines" style="margin-bottom:10px;" > Philippines</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="France" style="margin-bottom:10px;"> France</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Hong Kong" style="margin-bottom:10px;"> Hong Kong</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="India" style="margin-bottom:10px;" > India</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Indonesia" style="margin-bottom:10px;"> Indonesia</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Japan" style="margin-bottom:10px;"> Japan</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Korea" style="margin-bottom:10px;" > Korea</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Taiwan" style="margin-bottom:10px;"> Taiwan</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Thailand" style="margin-bottom:10px;"> Thailand</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Italy" style="margin-bottom:10px;" > Italy</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Germany" style="margin-bottom:10px;"> Germany</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Greece" style="margin-bottom:10px;"> Greece</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Turkey" style="margin-bottom:10px;" > Turkey</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Malaysia" style="margin-bottom:10px;"> Malaysia</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Israel" style="margin-bottom:10px;"> Israel</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Belgium" style="margin-bottom:10px;" > Belgium</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Australia" style="margin-bottom:10px;"> Australia</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Netherlands" style="margin-bottom:10px;"> Netherlands</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Portugal" style="margin-bottom:10px;" > Portugal</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Brazil" style="margin-bottom:10px;"> Brazil</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Spain" style="margin-bottom:10px;"> Spain</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Mexico" style="margin-bottom:10px;" > Mexico</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Czech Republic" style="margin-bottom:10px;"> Czech Republic</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Norway" style="margin-bottom:10px;"> Norway</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Sweden" style="margin-bottom:10px;" > Sweden</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Hungary" style="margin-bottom:10px;"> Hungary</div>
        <div style="float:left;"><input type="checkbox" name="nation" value="Russia" style="margin-bottom:10px;"> Russia</div>
        <br style="clear:both;">
      </div>
    </div>

    <br style="clear:both;">
    <div>
      <h4>Genre</h4>
      <div style="background:rgba(255,255,255,0.5);padding:10px;padding-bottom:5px;" id="genres">
        <div style="float:left;"><input type="checkbox" name="genre" value="Action" style="margin-bottom:10px;"> Action</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Adventure" style="margin-bottom:10px;"> Adventure</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Animation" style="margin-bottom:10px;"> Animation</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Comedy" style="margin-bottom:10px;"> Comedy</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Drama" style="margin-bottom:10px;"> Drama</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Family" style="margin-bottom:10px;"> Family</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Fanstasy"  style="margin-bottom:10px;"> Fanstasy</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Horror" style="margin-bottom:10px;"> Horror</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Mystery" style="margin-bottom:10px;"> Mystery</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Romance"  style="margin-bottom:10px;"> Romance</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Sci-fi" style="margin-bottom:10px;"> Sci-fi</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Thriller" style="margin-bottom:10px;"> Thriller</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Fanstasy"  style="margin-bottom:10px;"> War</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Documentary" style="margin-bottom:10px;"> Documentary</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Western" style="margin-bottom:10px;"> Western</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Musical" style="margin-bottom:10px;" > Musical</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Biography" style="margin-bottom:10px;"> Biography</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Sport" style="margin-bottom:10px;"> Sport</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Crime" style="margin-bottom:10px;"> Crime</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="History" style="margin-bottom:10px;"> History</div>
        <div style="float:left;"><input type="checkbox" name="genre" value="Film Pendek" style="margin-bottom:10px;"> Film Pendek</div>
        <br style="clear:both;">

      </div>

    </div>

    <button style="height:8%;width:15%; margin-left:85%;margin-top: 10px;background: #686868;color: #fff;border:none;" onclick="filterFilm()">Filter Movies</button>

  </div>
  <br>
  <div id="result" style="padding-bottom: 30px;" >

  </div>

  <div id="next" style="position:relative;" >
    <div style="position:absolute;left:50%; transform: translate(-50%, 0);height:40px;">
      <button onclick="nextPage(1)" class="btnNext"><<</button>
      <button onclick="nextPage(this.innerHTML)" class="btnNext" id="prev">4</button>
      <button onclick="nextPage(this.innerHTML)" class="btnNext" id="current" >5</button>
      <button onclick="nextPage(this.innerHTML)" class="btnNext" id="nextBtn" >6</button>
      <button onclick="nextPage(-1)" class="btnNext">>></button>
    </div>
  </div>
</div>
