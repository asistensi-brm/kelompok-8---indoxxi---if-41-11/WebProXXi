<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Login & Register</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css' ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/login-register.css' ?>" />
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php echo base_url().'assets/js/login-register.js' ?>"></script>
	<script>
	window.onload = function() {
		//console.log(window.localStorage.getItem("token"));
		if(window.localStorage.getItem("token") != null){
			console.log("on working");
			$.post( "http://localhost:3000/isLogin", { token: window.localStorage.getItem("token")}).done(function( data ) {
				console.log(data);
				window.localStorage.setItem("uid" , data.id);
				if(data.isLoggedin){
					window.location.href = "./home"
				}
			});
		}
	}


	</script>
</head>
<body style="background:linear-gradient(to bottom, #845422, #6a3b24, #4b2621, #291718, #000000);">
	<div class="register" style="width:100%; height:100%; margin:0; padding-right:0;">
	                <div class="row" >
	                    <div class="col-md-3 register-left">
	                        <img src="<?php echo base_url().'assets/img/login/movie_creation_white_192x192.png' ?>" alt=""/>
	                        <h4>Welcome to Indoxii</h4>
	                        <p>Get benefits with become our member!</p>
	                        <a href="<?php echo base_url('home') ?>"><input type="button" value="Home" style="color: #845422;"/><br/></a>
	                    </div>


	                    <div class="col-md-9 register-right" style="padding-right: 2%;">
													<div class="toogle-lr" >
														<a class="unactive" style="color: #fff;cursor: pointer;" onclick="changeSesion(this , this.parentElement , this.parentElement.parentElement.children[1])">Register</a>
														<a class="active" style="color: #845422;cursor: pointer;" onclick="changeSesion(this , this.parentElement, this.parentElement.parentElement.children[1])">Login</a>
													</div>

													<div class="tab-content" id="myTabContent" name="Login" >
		                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab" style="height:470px;">
		                                <h3 class="register-heading">Login to Indoxxi</h3>
																		<form action="<?php echo base_url().'login/login'; ?>" method="post">
			                                <div class="row register-form" style="text-align:center;">

																				<div class="form-group" style="width:200%;">
																						<input type="text" class="form-control" placeholder="Username *" name="username" value="" />
																				</div>
																				<div class="form-group" style="width:200%;">
																						<input type="password" class="form-control" placeholder="Password *" name="password" value="" />
																				</div>

																				<input type="submit" class="btnRegister" value="Login" style="margin: auto;width:200px;height:50px;padding:0;"/>
			                                </div>
																		</form>
																		<?php
																		if(!empty($this->session->flashdata('error'))){
																		?>
																			<div style="background:inherit; border-radius:0.4em;border:2px solid #d11b27;color:#d11b27; padding-left:2%;margin-left:4%;">
	 		 																	<?php echo $this->session->flashdata('error'); ?>
	 		 																</div>
																		<?php
																		}
																		?>
		                            </div>



																<div class="tab-pane fade show " id="home" role="tabpanel" aria-labelledby="home-tab" style="height:470px;">
																	 <h3 class="register-heading">Register to Indoxxi</h3>
																	 <form action="<?php echo base_url().'login/register'; ?>" method="post">
																		 <div class="row register-form">
																				 <div class="col-md-6" >
																						 <div class="form-group">
																								 <input type="text" class="form-control" placeholder="Your Name *" name="nama" value="" />
																						 </div>
																						 <div class="form-group">
																								 <input type="text" class="form-control" placeholder="Username *" name="username" value="" />
																						 </div>
																						 <div class="form-group">
																								 <input type="password" class="form-control" placeholder="Password *" name="password" value="" />
																						 </div>
																						 <div class="form-group">
																								 <input type="password" class="form-control"  placeholder="Confirm Password *" name="cpassword" value="" />
																						 </div>

																				 </div>
																				 <div class="col-md-6">
																						 <div class="form-group">
																								 <input type="email" class="form-control" placeholder="Your Email *" name="email" value="" />
																						 </div>
																						 <div class="form-group">
																								 <input type="text" minlength="10" maxlength="13" class="form-control" placeholder="Your Phone *" name="hp" value="" />
																						 </div>
																						 <div class="form-group">
																								 <div class="maxl">
																										 <label class="radio inline">
																												 <input type="radio" name="gender" value="male" checked>
																												 <span> Male </span>
																										 </label>
																										 <label class="radio inline">
																												 <input type="radio" name="gender" value="female">
																												 <span>Female </span>
																										 </label>
																								 </div>
																						 </div>

																				 </div>
																				 <input type="submit" class="btnRegister"  value="Register" style="margin: auto;width:200px;height:50px;padding:0;"/>
																		 </div>
																	 </form>

															 </div>

															</div>



	                    </div>




	                </div>

	 </div>

</body>
</html>
