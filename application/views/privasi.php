<h1>Kebijakan  Privasi / Privacy Police</h1>

<div class="badge" >
	<div>
		<img src="assets/img/icon/indo.png" alt="bendera" style="border : 0; vertical-align:middle;width:auto; height:28px;"/>
		
		Bahasa Indonesia
	</div>
</div>

	<p>
		IndoXXI atau XX1 tidak memiliki atau menyimpan konten di server sendiri, hanya mengambil tautan atau embed konten dari luar yang telah di upload ke situs-situs website populer seperti Youtube, Google Drive, Dailymotion, ZShare dan sejenisnya.
	</p>
	<p>
		Semua merk dagang, video, nama dagang, karya cipta, logo yang digunakan merupakan kepunyaan dari masing-masing pemiliknya / perusahaannya. IndoXXI atau XX1 tidak bertanggungjawab untuk apa yang orang lain upload ke situs pihak ketiga. Kami memastikan kepada setiap pemilik hak cipta bahwa setiap tautan-tautan anda berada di tempat lain dan video yang di embed juga dari beberapa situs seperti yang telah disebutkan di atas. Jika anda memiliki isu legalitas, silahkan hubungi pemilik konten media yang bersangkutan. 
	</p>
	<p>
		Kebijakan privasi ini dimaksudkan untuk memberitahu informasi tentang tipe-tipe informasi yang diambil oleh IndoXXI atau XX1 ketika anda mengunjunginya. 
	</p>
	<p>
		LOG FILES
		Kami dapat dengan otomatis melalui tracking services pihak ketiga seperti Google Analytics mengambil data informasi non-pribadi tertentu tentang penggunaan anda di dalam website dan menuliskannya di file log. Informasi ini dapat seperti IP address, browser type, internet service provider (ISP), referring/exit pages, operating system, date/timestamps, dan data yang berhubungan. Kami menggunakan informasi-informasi tersebut yang tidak mengidentifikasi setiap pengguna hanya untuk meningkatkan kualitas dari layanan kami. Karena menghormati privasi setiap pengguna, kami tidak menghubungkan pengambilan data otomatis ini ke informasi pribadi. 
	</p>
	<p>
		COOKIES 
		Cookies adalah serangkaian teks yang disimpan pada komputer anda oleh situs web yang anda kunjungi. Pada umumnya cookie menyimpan pengaturan atau preferensi Anda untuk suatu situs web tertentu, misalnya bahasa yang dipilih, atau lokasi (negara) Anda. Kami menggunakan Cookies untuk memudahkan anda untuk menggunakan layanan yang ditawarkan oleh kami, dan untuk meningkatkan user experience. Pelacakan cookie tersimpan di hard drive anda untuk jangka waktu yang lama. Anda dapat dengan bebas untuk menolak cookie, tetapi dengan begitu, anda tidak bisa mendapatkan manfaat maksimal dari yang kami tawarkan. Kita tidak menghubungkan informasi yang disimpan di cookies dengan informasi pribadi anda yang telah diserahkan ketika menggunakan layanan kami. 
	</p>
	
	<p>
		Changes to Privacy Policy
		Kebijakan privasi ini sewaktu-waktu bisa berubah. Jika anda adalah pengguna dari IndoXXI atau XX1, kami akan memberitahukan jika ada perubahan terkait dengan kebijakan privasi ini. Jika tidak, anda dapat melihatnya secara langsung kebijakan privasi terakhir di website IndoXXI atau XX1
	</p>

<div class="badge">
	<div>
		<img src="assets/img/icon/indo.png" alt="bendera" style="border : 0; vertical-align:middle;width:auto; height:28px;"/>
		
		English
	</div>
</div>
	<p>
		IndoXXI or XX1 doesn't host any content on it own server and just linking to or embedding content that was uploaded to popular Online Video hosting sites like dailymotion.com, Youtube.com, Google Drive, ZShare.net, cloudy, netu.tv and such sites. 
	</p>
	<p>
		All trademarks, Videos, trade names, service marks, copyrighted work, logos referenced herein belong to their respective owners/companies. IndoXXI or XX1 is not responsible for what other people upload to 3rd party sites. We urge all copyright owners, to recognize that the links contained within this site are located somewhere else on the web or video embedded are from other various site like included above!. If you have any legal issues please contact appropriate media file owners / hosters. 
	</p>
	<p>
		This privacy policy is intended to inform you about the types of information gathered by us when you visit Indoxxi or XX1. 
	</p>
	<p>
		LOG FILES 
		We may automatically and through third-party tracking services (e.g., Google Analytics) gather certain non-personally identifiable information about your use of IndoXXI or XX1 and store it in log files. This information may include internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, operating system, date/time stamps, and related data. We use this information, which does not identify individual users, solely to improve the quality of our services. Out of respect for your privacy, we do not link this automatically-collected data to personally identifiable information. 
	</p>
	<p>
		COOKIES 
		A cookie is a small text file that is stored on a user's computer for record-keeping purposes. We use both session ID cookies and tracking cookies. We use session cookies to make it easier for you to use IndoXXI or XX1. A session ID cookie expires when you close your browser. We use tracking cookies to better understand how you use IndoXXI or XX1, and to enhance your user experience. A tracking cookie remains on your hard drive for an extended period of time. You are free to decline cookies, but by doing so, you may not be able to take full advantage of all of our offerings. We do not link the information we store in cookies to any personally identifiable information you submit while using IndoXXI or XX1 
	</p>
	<p>
		Changes to Privacy Policy 
		We may periodically update this policy. If you are a IndoXXI or XX1 member, we will attempt to notify you of material updates by sending a notice to an email address you may have provided to us. Otherwise, you may view the updated version of this policy on the IndoXXI or XX1 website.
	</p>