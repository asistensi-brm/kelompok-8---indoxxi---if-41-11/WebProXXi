<body>
  <div class="movie-content">
    <!--BREADCRUMB [breadcrumb: Home / Layarkaca21 / genre / year / title]-->
    <nav aria-label="breadcrumb" style="background-color: #EEEEEE">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url()."index.php/home" ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="#"><?php echo $film->genre ?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $film->judul ?></li>
      </ol>
    </nav>
    <!--MOVIE THUMBNAIL-->
    <center>

      <!--<video class="movie-placeholder">
        <source src="https://streamcherry.com/embed/rbrddklcbkocmaoo/" type="video/mp4">
      </video>-->
      <!--<iframe src="<?php echo $film->url_video ?>" width="700" height="430" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" scrolling="no" frameborder="0"></iframe>
      -->

      <iframe class="movie-placeholder" src="<?php echo $film->url_video ?>" width="100%" height="405px"></iframe>

    </center>

    <!--TITLE [label: FILM RATING]-->
    <h2><?php echo $film->judul ?></h2>
    <hr class="hr-gold">

    <!--movie description-->
    <p>
      <?php echo $film->sinopsis ?>
    </p>
    <!--movie poster-->
    <div class="row">
      <div class="col-2">
        <div class="poster-placeholder" style="background-image: url('<?php echo $film->url_cover ?>'); background-size: cover;">
        </div>
      </div>
      <!--movie information (genre, actors, directors, production, duration, quality, release date, countries)-->
      <div class="col-6">
        <table class="table table-borderless">
          <tr>
            <td>Genre: <?php echo $film->genre ?></td>
          </tr>
          <tr>
            <td>Actors: <?php echo $film->actors ?></td>
          </tr>
          <tr>
            <td>Directors: <?php echo $film->directors ?></td>
          </tr>
          <tr>
            <td>Production: <?php echo $film->production ?></td>
          </tr>
        </table>
      </div>
      <div class="col-4">
        <table class="table table-borderless">
          <tr>
            <td>Duration: <?php echo $film->durasi ?></td>
          </tr>
          <tr>
            <td>Countries: <?php echo $film->nation ?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>

          <tr >
            <td>
              <button id='btnWish' onclick="editToWishlist(this , <?php echo $film->id ?>);" type="button" name="button" class="btnWishlistFocusFilm" title="Click to add this movie to your wishlist">❤ Wishlist This Movie!</button>
            </td>
          </tr>

        </table>
      </div>
    </div>
    <!--related search query-->
    <hr class="hr-black">
  </div>
</body>
<script type="text/javascript">
  function onLoad(){
    console.log("onloda");
    obj = document.getElementById('btnWish');
    let sendData = {uid : window.localStorage.getItem('uid') , fid : <?php echo $film->id ?>}
    console.log(sendData);
    if(window.localStorage.getItem('uid') != -1){//check if loggedin
      $.post( "http://localhost:3000/isInWishlist", sendData ).done(function( data ) {

        if(data.success){
          setToAdded(obj);
        }else{
          setToRemoved(obj);
        }
      });
    }else{
      setToRemoved(obj);

    }


  }
  function editToWishlist(obj , idFilm){

    if(window.localStorage.getItem('uid') == -1){//check if loggedin
      alert('you must logedin for add to wishlist');
      return;
    }

    let sendData = {uid : window.localStorage.getItem('uid') , fid : idFilm}

    if(!obj.className.includes('active')){

      $.post( "http://localhost:3000/addToWishlist", sendData ).done(function( data ) {
        if(data.success){
          setToAdded(obj);
        }else{
          setToRemoved(obj);
        }
      });
    }else{
      console.log("data");
      $.post( "http://localhost:3000/removeFromWishlist", sendData ).done(function( data ) {
        if(data.success){
          setToRemoved(obj);
        }else{
          setToAdded(obj);
        }
      });
    }

  }
  function setToAdded(obj){
    obj.className += "active"
    obj.innerHTML = "❤ Remove From Wishlist"
  }
  function setToRemoved(obj){
    obj.className = obj.className.replace("active" , "");
    obj.innerHTML = "❤ Wishlist This Movie!"
  }
</script>
