<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller {



	public function __construct() {
		parent::__construct();
	 	$this->load->model('M_Movie');
	}

	public function _remap($param) {
    $this->index($param);
  }

  public function index($judul) {
		$data['film'] = $this->M_Movie->getMovieID($judul);

		$this->session->set_flashdata('judulFilm', $data['film']->judul);
		$this->load->view('head_detail');
		$this->load->view('pageheader');
		$this->load->view('detail', $data);
		$this->load->view('pagefooter');
  }

}
