<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	 public function __construct()
	 {
		 parent::__construct();
		 /*
			 ..:: QUESTION ::..
			 Doing Load Model M_web
		 */
		 // YOUR_CODE_HERE
		 $this->load->model('M_login');
	 }

	public function index()
	{

		$this->load->view('login');
	}

	public function login()
	{
		$user = $this->input->post('username');
		$pass = $this->input->post('password');

		$result = $this->M_login->login($user , $pass);
		$baseurl = base_url();
		//echo "<script>console.log('$baseurl"."home');</script>";

		if(count($result) == 0 || count($result) > 1){
			//echo "<script>console.log('es1');</script>";
			//echo "<script>console.log('es2');window.location.href ='$baseurl"."login/index';</script>";
			//echo "<script>alert('login failed');</script>";
			$this->session->set_flashdata('error', 'Login Failed');
			redirect('login/index');
		}else{
			echo "<script>alert('loginSuccess');window.location.href ='$baseurl"."home';</script>";
		}


	}
	public function register()
	{
		$user = $this->input->post('username');
		$pass = $this->input->post('password');
		$name = $this->input->post('nama');
		$cpass = $this->input->post('cpassword');
		$email = $this->input->post('email');
		$gender = $this->input->post('gender');
		$hp = $this->input->post('hp');
		echo ($pass != $cpass);
		if($pass != $cpass){
			redirect('login/index');
		}


		$result = $this->M_login->register($name , $user , $pass , $email , $gender , $hp);

		if($result['result']){
			redirect('home');
		}else{
			redirect('login/index');
		}



	}
}
