<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privasi extends CI_Controller {

  public function index() {
  		$this->load->view('head_privasi');
  		$this->load->view('pageheader');
		$this->load->view('privasi');		
		$this->load->view('pagefooter');
  }
  public function dmca()
  {
  		$this->load->view('head_privasi');
  		$this->load->view('pageheader');
		$this->load->view('dmca');		
		$this->load->view('pagefooter');
  }

}
