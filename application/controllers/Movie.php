<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Movie extends CI_Controller {



	public function __construct() {
		parent::__construct();
	 	$this->load->model('M_Movie');
	}

	public function _remap($param) {
    $this->index($param);
  }

  public function index($judul) {
		$data['film'] = $this->M_Movie->getMovie(urldecode($judul));

		$this->M_Movie->trendUp($data['film']->id);

		$this->session->set_flashdata('judulFilm', $data['film']->judul);
		$this->load->view('head_movie_focus');
		$this->load->view('pageheader');
		$this->load->view('movie_focus', $data);
		$this->load->view('pagefooter');
  }

}
