<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		//$this->load->view('welcome_message');
		$this->load->view('head_home');
		$this->load->view('pageheader');
		$this->load->view('home');
		$this->load->view('pagefooter');



	}
}
