<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct() {
		parent::__construct();
	 	$this->load->model('M_profile');
	}
	public function index()
	{
		$this->load->view('profile');
	}
	public function change_password()
	{
		$this->load->view('change_password');
	}
	public function updateData()
	{
		if($this->M_profile->isCorrectPass($this->input->post('id'),$this->input->post('opassword'))){

				$data = array('nama' => $user = $this->input->post('nama'),
								'uname' => $user = $this->input->post('username'),
								'email' => $user = $this->input->post('email'),
								'gender' => $user = $this->input->post('gender'),
								'nomor' => $user = $this->input->post('hp'),
							  'id' => $user = $this->input->post('id')
							  );

				$this->M_profile->updateData($data);

				if($this->input->post('passChange') == "Yes" && ($this->input->post('password')==$this->input->post('cpassword')) ){
					$data2 = array('password' => $this->input->post('password'),'id' => $user = $this->input->post('id'));
					$this->M_profile->updatePass($data2);
				}else if($this->input->post('passChange') == "Yes"){
					echo "<script>alert('Data Edited but Password Failed Update confirm Password not match');window.location.href ='".base_url()."profile';</script>";
				}
		}

		echo "<script>alert('Data Edited Succesfully');window.location.href ='".base_url()."profile';</script>";
			//$this->load->view('change_password');
	}
}
