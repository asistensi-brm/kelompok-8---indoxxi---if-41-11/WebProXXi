<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_movie extends CI_Model {

  public function getMovie($judul) {
    $this->db->where('judul', $judul);
    $query = $this->db->get('film');

    return $query->row();
  }
  public function getMovieID($judul) {
    $this->db->where('id', $judul);
    $query = $this->db->get('film');

    return $query->row();
  }

  public function trendUp($id) {
    $this->db->set('trending', 'trending+1', FALSE);
    $this->db->where('id', $id);
    $this->db->update('film');
  }
}
?>
