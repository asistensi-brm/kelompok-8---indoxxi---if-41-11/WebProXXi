-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2019 at 05:49 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `indoxxi`
--

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE `film` (
  `judul` varchar(80) NOT NULL,
  `sinopsis` varchar(800) NOT NULL,
  `durasi` varchar(6) NOT NULL,
  `id` int(11) NOT NULL,
  `url_cover` varchar(200) NOT NULL,
  `url_video` varchar(200) NOT NULL,
  `rating` float NOT NULL,
  `genre` varchar(200) NOT NULL,
  `nation` varchar(20) NOT NULL,
  `directors` varchar(30) NOT NULL,
  `production` varchar(50) NOT NULL,
  `actors` varchar(100) NOT NULL,
  `upload_time` datetime NOT NULL,
  `trending` int(11) NOT NULL,
  `download_url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`judul`, `sinopsis`, `durasi`, `id`, `url_cover`, `url_video`, `rating`, `genre`, `nation`, `directors`, `production`, `actors`, `upload_time`, `trending`, `download_url`) VALUES
('The Silence (2019)', 'With the world under attack by deadly creatures who hunt by sound, a teen and her family seek refuge outside the city and encounter a mysterious cult.', '90m', 1, 'https://image.tmdb.org/t/p/w185/lTVOquzxw2DPF3MKuYd1ynz9F6H.jpg', 'https://streamango.com/embed/loqcctffqefcfkme/', 6.9, 'Horror', 'Germany,USA', 'John R. Leonetti', 'Constantin Film,Emjag Producti', 'Kiernan Shipka, Stanley Tucci,', '2019-04-18 12:40:00', 5, 'https://www12.zippyshare.com/v/Xt9RLXB8/file.html\r\n'),
('The Silence (2019)', 'With the world under attack by deadly creatures who hunt by sound, a teen and her family seek refuge outside the city and encounter a mysterious cult.', '90m', 2, 'https://image.tmdb.org/t/p/w185/lTVOquzxw2DPF3MKuYd1ynz9F6H.jpg', 'https://streamango.com/embed/loqcctffqefcfkme/', 6.9, 'Horror', 'Germany,USA', 'John R. Leonetti', 'Constantin Film,Emjag Producti', 'Kiernan Shipka, Stanley Tucci,', '2019-04-18 12:45:00', 7, 'https://www12.zippyshare.com/v/Xt9RLXB8/file.html\r\n'),
('The Silence (2019)', 'With the world under attack by deadly creatures who hunt by sound, a teen and her family seek refuge outside the city and encounter a mysterious cult.', '90m', 3, 'https://image.tmdb.org/t/p/w185/lTVOquzxw2DPF3MKuYd1ynz9F6H.jpg', 'https://streamango.com/embed/loqcctffqefcfkme/', 6.9, 'Horror', 'Germany,USA', 'John R. Leonetti', 'Constantin Film,Emjag Producti', 'Kiernan Shipka, Stanley Tucci,', '2019-04-18 12:42:00', 0, 'https://www12.zippyshare.com/v/Xt9RLXB8/file.html\r\n'),
('Fall in Love at First Kiss (2019)', 'After an earthquake destroys Xiang Qin\'s house, she and her father move in with the family of her father\'s college buddy. To her surprise, her new kind and amicable aunt and uncle are the parents of her cold and distant schoolmate, Jiang Zhi Shu, a genius with an IQ of 200 whom not too long ago rejected her when she confessed her feelings for him. Will the close proximity give her a second chance to win Zhi Shu\'s heart? Or will her love for him end under his cold words?', '121m', 4, 'https://image.tmdb.org/t/p/w185/wtaSH8MfJSCEIrrEX9SQuHdU5sl.jpg', 'https://streamango.com/embed/kfrprdobltropeqf/', 6, 'Comedy, Romance', 'Taiwan, China', 'Yu Shan Chen', '-', 'Darren Wang, Jelly Lin, Li-tung Chang', '2019-04-20 00:00:00', 0, 'https://www12.zippyshare.com/v/iO4oCpOs/file.html'),
('Warfighter', 'Rusty Wittenburg is a Navy SEAL struggling to balance his family life and his job. He fights daily to maintain the line between reality and the nightmares his PTSD conjures up for him. Dedicated to his team and his mission, he is willing to give the ultimate sacrifice for his fellow brothers and teammates. ', '105m', 5, 'https://image.tmdb.org/t/p/w185/ufSLDs841q6fJXLd0IcPTQq7Uxb.jpg', 'https://streamango.com/embed/sdtmntlsrdpmlrrn/', 7.7, 'Action, Drama, Romance', 'USA', 'Jerry G. Angelo', 'Fire Born Studios', 'Jerry G. Angelo, Paul Logan, Isaac C. Singleton Jr.', '2019-04-20 09:31:07', 1, 'https://www12.zippyshare.com/v/Ah7TnRcz/file.html'),
('Gully Boy', 'A coming-of-age story based on the lives of street rappers in Mumbai. ', '154m', 6, 'https://image.tmdb.org/t/p/w185/4RE7TD5TqEXbPKyUHcn7CSeMlrJ.jpg', 'https://streamango.com/embed/smctnkanesadmtks/', 8.5, 'Drama, Music', 'India', 'Zoya Akhtar', 'Excel Entertainment, Tiger Bab', 'Ranveer Singh, Alia Bhatt, Siddhant Chaturvedi', '2019-04-21 00:00:00', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `nama` varchar(40) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `id` int(11) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `token` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`nama`, `username`, `password`, `email`, `id`, `gender`, `phone`, `token`) VALUES
('firdi', 'firdiar', 'c4ntik', 'firdi.ansyah20@gmail.com', 1, 'Male', '0812345678', '123'),
('lius', 'lius', 'lius', 'lius@gmail.com', 2, 'Male', '08123456789', '123'),
('firdiansyah', '123', '1234', 'firdi.ansyah20@gmail.com', 5, 'male', '123123123123', 'g1mM60Hs79YLbhpoty'),
('firdiansyah ramadhan', 'firdiar1231', '1231', 'firdi.ansyah20@gmail.com1', 6, 'female', '1231231231231', '1231');

-- --------------------------------------------------------

--
-- Table structure for table `whistlist`
--

CREATE TABLE `whistlist` (
  `comb` varchar(22) NOT NULL,
  `uid` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `whistlist`
--

INSERT INTO `whistlist` (`comb`, `uid`, `fid`, `datetime`) VALUES
('5,1', 5, 1, '2019-04-18 10:08:38'),
('5,3', 5, 3, '2019-04-19 07:34:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whistlist`
--
ALTER TABLE `whistlist`
  ADD PRIMARY KEY (`comb`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
