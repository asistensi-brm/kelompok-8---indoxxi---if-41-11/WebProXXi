let searchData;
let countPage = 1;

function onLoad(){
  //loadAllFilm();
  if(window.sessionStorage.getItem('search')!="" && window.sessionStorage.getItem('search')!=null){

    searchData = JSON.parse(window.sessionStorage.getItem('search'));
  }else{
    searchData = {
      name : "",
      nation :"",
      genre : "",
      orderby : "terbaru",
      uid : window.localStorage.getItem("uid"),
      page : 1
    }
    window.sessionStorage.setItem('search', JSON.stringify(searchData));
  }
  console.log(searchData);
  $.post( "http://localhost:3000/searchFilm", searchData ).done(function( data ) {
    console.log(data);
    let holder = document.getElementById('result');
    for(let i = 0 ; i < data.data.length; i++){
      addKotakFilm(holder , data.data[i]);
    }
    if(searchData.page != 1){
      document.getElementById('prev').innerHTML = searchData.page-1;
    }else{
      document.getElementById('prev').remove();
    }
      document.getElementById('current').innerHTML = searchData.page;
    if(searchData.page != data.count){
      document.getElementById('next').innerHTML = searchData.page+1;
    }else{
      document.getElementById('nextBtn').remove();
    }
    countPage = data.count;


    updateTag(searchData);

  });


}

function updateTag(sData){
  if(sData.name == ""){
    document.getElementById('nam').remove();
  }else{
    document.getElementById('nam').children[0].innerHTML = "Hasil Pencarian '"+sData.name+"'";
  }
  if(sData.orderby == ""){
    document.getElementById('ord').remove();
  }else{
    document.getElementById('ord').children[0].innerHTML = "Order By "+sData.orderby;

    let genres = document.getElementById('orderby');
    let str = sData.orderby.split(",");
    for(let j= 0; j < str.length; j++){
      for(let i = 0; i < genres.childElementCount; i++){
          if(genres.children[i].value == str[j]){
            genres.children[i].checked = true;
          }else{
            genres.children[i].checked = false;
          }
      }
    }
  }
  if(sData.genre == ""){
    document.getElementById('genr').remove();
  }else{
    document.getElementById('genr').children[0].innerHTML = "Genres "+sData.genre;

    let genres = document.getElementById('genres');
    let str = sData.genre.split(",");
    for(let j= 0; j < str.length; j++){
      for(let i = 0; i < genres.childElementCount-1; i++){
          if(genres.children[i].children[0].value == str[j]){
            genres.children[i].children[0].checked = true;
          }
      }
    }
  }
  if(sData.nation == ""){
    document.getElementById('nat').remove();
  }else{
    document.getElementById('nat').children[0].innerHTML = "Nation "+sData.nation;

    let genres = document.getElementById('nations');
    let str = sData.nation.split(",");
    for(let j= 0; j < str.length; j++){
      for(let i = 0; i < genres.childElementCount-1; i++){
          //console.log(str[j]+" = "+genres.children[i].children[0].value);
          if(genres.children[i].children[0].value == str[j]){
            genres.children[i].children[0].checked = true;
          }
      }
    }
  }

}

function addKotakFilm(holder , data){

  let file = "./assets/html-elm/film_kotak.html";
  if (file) {
    /* Make an HTTP request using the attribute value as the file name: */
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4) {
        if (this.status == 200) {
          let komen = $(this.responseText);

          komen[0].querySelector("#rating").innerHTML = data.rating;
          komen[0].querySelector("#duration").innerHTML = data.durasi;
          komen[0].querySelector("#judul").innerHTML = data.judul;
          komen[0].querySelector("#image-cover").setAttribute("src",data.url_cover);
          komen[0].setAttribute("name",data.id);
          holder.append(komen[0]);
        }

      }
    }
    xhttp.open("GET",file, true);
    xhttp.send();
    return;
  }
}
function opencloseFilter(){
  console.log("open close");
  let filter = document.getElementById('filter');
  if(filter.style.display == "none"){
    filter.style.display = "block";
  }else{
    filter.style.display = "none";
  }
}

function nextPage(page){
  if(page == -1){
    page = countPage;
  }
  searchData.page= page;
  window.sessionStorage.setItem('search', JSON.stringify(searchData));
  window.location.href = "./search";

}

function filterFilm(){
  let order = document.getElementById('orderby');
  let nations = document.getElementById('nations');
  let genre = document.getElementById('genres');

  for(let i = 0; i < order.childElementCount; i++){
      if(order.children[i].checked){
        searchData.orderby = order.children[i].value;
        break;
      }
  }
  let j = 0;
  let nat = [];
  for(let i = 0; i < nations.childElementCount; i++){
      if(nations.children[i].children[0]!= null && nations.children[i].children[0].checked){

        nat[j] = nations.children[i].children[0].value;
        j++;
      }
  }
  searchData.nation = nat.join(",");
  j = 0;
  nat = [];
  for(let i = 0; i < genres.childElementCount; i++){
      if(genres.children[i].children[0] != null && genres.children[i].children[0].checked){
        nat[j] =  genres.children[i].children[0].value;
        j++;
      }
  }
  searchData.genre = nat.join(",");

  searchData.page= 1;
  searchData.uid = window.localStorage.getItem("uid");
  console.log(searchData);
  window.sessionStorage.setItem('search', JSON.stringify(searchData));
  window.location.href = base_url+"search";

}
function FocusFilm(obj){
    window.location.href = "detail/"+obj.getAttribute("name");
}
