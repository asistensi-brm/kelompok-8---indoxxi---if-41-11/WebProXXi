let base_url = "";

function setbaseurl(url){
  console.log(url)
  base_url = url;
}

function toPageSearch(data){
  window.sessionStorage.setItem('search', JSON.stringify(data));
  window.location.href = base_url+"search";
}
function toPageSearchbyName(name){
  data = {
    name : name,
    nation :"",
    genre : "",
    orderby : "terbaru",
    uid : window.localStorage.getItem("uid"),
    page : 1
  }
  console.log(name);
  window.sessionStorage.setItem('search', JSON.stringify(data));
  window.location.href = base_url+"search";
}
function toPageSearchbyGenre(name){
  data = {
    name : "",
    nation :"",
    genre : name,
    orderby : "terbaru",
    uid : window.localStorage.getItem("uid"),
    page : 1
  }
  console.log(name);
  window.sessionStorage.setItem('search', JSON.stringify(data));
  window.location.href = base_url+"search";
}
function toPageSearchbyNation(name){
  data = {
    name : "",
    nation :name,
    genre : "",
    orderby : "terbaru",
    uid : window.localStorage.getItem("uid"),
    page : 1
  }
  console.log(name);
  window.sessionStorage.setItem('search', JSON.stringify(data));
  window.location.href = base_url+"search";
}
function toPageSearchbyOrder(name){
  data = {
    name : "",
    nation :"",
    genre : "",
    orderby :  name,
    uid : window.localStorage.getItem("uid"),
    page : 1
  }
  console.log(name);
  window.sessionStorage.setItem('search', JSON.stringify(data));
  window.location.href = base_url+"search";
}
