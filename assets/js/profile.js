let currPage = 0;
function loadFilmWhistlist(id,page){
  currPage = page;
  $.get( "http://localhost:3000/getFilm/whistlist/"+id+"/"+page,function( data ) {
    let terbaruList = document.getElementById('whistlist');
    terbaruList.innerHTML = "";
    console.log(data);

    for(let i = 0 ; i < data.length ; i++){
      addKotakFilm(terbaruList,data[i]);
    }


    document.getElementById('current').innerHTML = page;

  });
}
function loadProfile(data){
  document.getElementById('dnama').innerHTML = data.nama;
  document.getElementById('duname').innerHTML = data.username;
  //document.getElementById('dpass').innerHTML = data.password;
  document.getElementById('demail').innerHTML = data.email;
  document.getElementById('dnomor').innerHTML = data.phone;
  document.getElementById('dgender').innerHTML = data.gender;

  document.getElementById('iid').value = data.id;
  document.getElementById('inama').value = data.nama;
  document.getElementById('iuname').value = data.username;
  //document.getElementById('iopass').value = data.password;
  document.getElementById('iemail').value = data.email;
  document.getElementById('inomor').value = data.phone;

  if(document.getElementById('igender').children[0].children[0].value == data.gender){
    document.getElementById('igender').children[0].children[0].checked = true;
  }else{
    document.getElementById('igender').children[1].children[0].checked = false;
  }
}
function loadData(data){
  loadProfile(data);
  loadFilmWhistlist(data.id,1);
}

function changeView(index){


  let card = document.getElementById('card');
  if(card.children[index].style.display == "block"){
    return;
  }
  for(let i = 0; i < card.childElementCount; i++){
    card.children[i].style.display = "none";
  }

  card.children[index].style.display = "block";

}

function addKotakFilm(holder , data){

  let file = "./assets/html-elm/film_kotak.html";
  if (file) {
    /* Make an HTTP request using the attribute value as the file name: */
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4) {
        if (this.status == 200) {
          let komen = $(this.responseText);
          komen[0].querySelector("#rating").innerHTML = data.rating;
          komen[0].querySelector("#duration").innerHTML = data.durasi;
          komen[0].querySelector("#judul").innerHTML = data.judul;
          komen[0].querySelector("#image-cover").setAttribute("src",data.url_cover);
          komen[0].setAttribute("name",data.id);
          komen[0].style.marginRight = "5%";
          holder.append(komen[0]);
        }

      }
    }
    xhttp.open("GET",file, true);
    xhttp.send();
    return;
  }
}
function FocusFilm(obj){
    //window.localStorage.setItem("mytime","123123");
    //var a = window.localStorage.getItem('token');
    //console.log(obj.getAttribute("name"));

    window.location.href = "detail/"+obj.getAttribute("name");
}
function nextPage(c){
  if((c > 0 && document.getElementById('whistlist').childElementCount < 24) || (currPage == 1 && c == -1)){
    alert("anda sudah mencapai ujung page");
    return;
  }
  currPage += c;
  loadFilmWhistlist(window.localStorage.getItem("uid"),currPage);
}
function Logout(){

  window.localStorage.setItem("token" , "");
    window.localStorage.setItem("uid" , -1);
    window.location.href = ".";

}
function toPageSearchbyOrder(name,base_url){
  console.log("123")
  data = {
    name : "",
    nation :"",
    genre : "",
    orderby :  name,
    uid : window.localStorage.getItem("uid"),
    page : 1
  }
  console.log(name);
  window.sessionStorage.setItem('search', JSON.stringify(data));
  window.location.href = base_url+"search";
}
