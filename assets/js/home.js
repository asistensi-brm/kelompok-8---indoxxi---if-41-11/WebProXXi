function FocusFilm(obj){
    window.location.href = "detail/"+obj.getAttribute("name");
}

function loadAllFilm(){

  console.log("Loading film")
  $.get( "http://localhost:3000/getFilm/terbaru/1",function( data ) {
    let terbaruList = document.getElementById('terbaru');
    console.log(data);
    //for(let j = 0 ; j < 10; j++){
      for(let i = 0 ; i < data.length ; i++){
        addKotakFilm(terbaruList,data[i]);
      }
    //}
  });
  $.get( "http://localhost:3000/getFilm/populer/1",function( data ) {
    let terbaruList = document.getElementById('populer');

    //for(let j = 0 ; j < 10; j++){
      for(let i = 0 ; i < data.length ; i++){
        addKotakFilm(terbaruList,data[i]);
      }
    //}
  });
  if(window.localStorage.getItem("uid")!=-1){
    $.get( "http://localhost:3000/getFilm/whistlist/"+window.localStorage.getItem("uid")+"/1",function( data ) {
      let terbaruList = document.getElementById('whistlist');
      console.log(data);
      //for(let j = 0 ; j < 10; j++){
        for(let i = 0 ; i < data.length ; i++){
          addKotakFilm(terbaruList,data[i]);
        }
      //}
    });
  }else{
    console.log("none")
    document.getElementById('btnWhislist').style.display = "none";
  }
}
function onLoad(){
  loadAllFilm();
}
function addKotakFilm(holder , data){

  let file = "./assets/html-elm/film_kotak.html";
  if (file) {
    /* Make an HTTP request using the attribute value as the file name: */
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4) {
        if (this.status == 200) {
          let komen = $(this.responseText);
          komen[0].querySelector("#rating").innerHTML = data.rating;
          komen[0].querySelector("#duration").innerHTML = data.durasi;
          komen[0].querySelector("#judul").innerHTML = data.judul;
          komen[0].querySelector("#image-cover").setAttribute("src",data.url_cover);
          komen[0].setAttribute("name",data.id);
          holder.append(komen[0]);
        }

      }
    }
    xhttp.open("GET",file, true);
    xhttp.send();
    return;
  }
}
function changeTab(tab , fillID){
  if(!tab.className.includes("Active")){
    for(let i=0; i<tab.parentElement.children.length;i++){
      console.log(tab.parentElement.children[i]);
      if(tab.parentElement.children[i].getAttribute("name") != null){
        tab.parentElement.children[i].className = "tabHome"
        document.getElementById(tab.parentElement.children[i].getAttribute("name")).style.display = "none";
      }
    }
    tab.className = "tabHomeActive";
    document.getElementById(tab.getAttribute("name")).style.display = "block";

  }
}
function more(obj){
  for(let i = 0; i <obj.childElementCount; i++){
    if(obj.children[i].className.includes("Active")){
      toPageSearchbyOrder(obj.children[i].name);
      break;
    }
  }
}
