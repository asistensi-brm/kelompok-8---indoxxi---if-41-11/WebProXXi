function changeSesion(btn , parent , content){
  if(btn.className == "unactive"){
    for(let i = 0 ; i < parent.children.length ; i++ ){
      if(parent.children[i].className == "unactive"){
        parent.children[i].className = "active"
        parent.children[i].style.color = '#845422'
      }else{
        parent.children[i].className = "unactive"
        parent.children[i].style.color = '#fff'
      }
    }

    for(let i = 0 ; i < content.children.length ; i++ ){
      if(content.children[i].className.includes("active")){
        content.children[i].className = content.children[i].className.replace("active", "");
      }else{
        content.children[i].className += "active"
      }
    }
  }


}
